
// Playing Cards
// Ryan Appel

#include <iostream>
#include <conio.h>

using namespace std;

enum class Suit
{
	Hearts,
	Spades,
	Diamonds,
	Clubs
};

enum class Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Suit Suit;
	Rank Rank;
};


int main()
{
	Card c1;
	c1.Rank = Rank::Ten;

	Card c2;
	c2.Rank = Rank::Ace;

	if (c2.Rank > c1.Rank) cout << "c2 is higher";

	(void)_getch();
	return 0;
}
